<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

/*
 * Custom RTE
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['teufels_presets'] = 'EXT:hive_thm_custom/Configuration/TsConfig/Page/Production/RTE.yaml';