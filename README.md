![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__thm__custom-blue.svg)
![version](https://img.shields.io/badge/version-11.5.*-yellow.svg?style=flat-square)

HIVE THEME CUSTOM
==========
(Basis) Hive Theme/Template Extension (Site Package)

#### This version supports TYPO3


![CUSTOMER](https://img.shields.io/badge/11_LTS-%23A3C49B.svg?style=flat-square)

#### Composer support
`composer req beewilly/hive_thm_custom`

## Contains
- Bootstrap 5.0
- jQuery 3.6
- GSAP 3

## Requirements
#### *installed in boilerplate
- `"b13/container"`
- `"buepro/typo3-container-elements"`
- `"svewap/ws-scss"`

## colPos Ranges
- (Hive) Partials/Templates : 2000-2999
- (Hive) Container : 1000-1999
- `"buepro/typo3-container-elements"` : 100-201


## Notice