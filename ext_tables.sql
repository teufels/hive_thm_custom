CREATE TABLE tt_content (
    tx_hiveovrttcontent_backendtitle varchar(255) DEFAULT '' NOT NULL,
    tx_hiveovrttcontent_anchortargetname varchar(255) DEFAULT '' NOT NULL,
    tx_hiveovrttcontent_customcssclass varchar(255) DEFAULT '' NOT NULL,
);