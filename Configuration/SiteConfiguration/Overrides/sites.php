<?php

$GLOBALS['SiteConfiguration']['site_language']['columns']['title']['config']['default'] = 'Deutsch';
$GLOBALS['SiteConfiguration']['site_language']['columns']['typo3Language']['config']['default'] = 'de';
$GLOBALS['SiteConfiguration']['site_language']['columns']['locale']['config']['default'] = 'de_DE.UTF-8';
$GLOBALS['SiteConfiguration']['site_language']['columns']['iso-639-1']['config']['default'] = 'de';
$GLOBALS['SiteConfiguration']['site_language']['columns']['navigationTitle']['config']['default'] = 'DE';
$GLOBALS['SiteConfiguration']['site_language']['columns']['hreflang']['config']['default'] = 'de-DE';
$GLOBALS['SiteConfiguration']['site_language']['columns']['flag']['config']['default'] = 'de';
$GLOBALS['SiteConfiguration']['site_language']['columns']['direction']['config']['default'] = 'ltr';