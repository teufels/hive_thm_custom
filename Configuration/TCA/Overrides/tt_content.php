<?php
defined('TYPO3_MODE') || die();

call_user_func(static function () {

    $additionalColumns = [
        'tx_hiveovrttcontent_anchortargetname' => [
            'label' => 'anchor target name',
            'description' => 'name (URL-format) for anchor target',
            'exclude' => 1,
            'config' => [
                'type' => 'input',
                'eval' => 'trim',
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ],
            ]
        ],
        'tx_hiveovrttcontent_customcssclass' => [
            'label' => 'custom css class',
            'description' => 'custom CSS classes (separated by space no leading dot)',
            'exclude' => 1,
            'config' => [
                'type' => 'input',
                'eval' => 'trim',
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ],
                'valuePicker' => [
                    'items' => [
                        ['Predefined Class', 'custom-class'],
                    ],
                ],
            ]
        ],
    ];

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'tt_content',
        $additionalColumns
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tt_content',
        '--div--;Extended,tx_hiveovrttcontent_anchortargetname,tx_hiveovrttcontent_customcssclass'
    );
});