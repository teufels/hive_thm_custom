<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}
$GLOBALS['TCA']['sys_file_reference']['columns']['crop']['config']['cropVariants'] = [
    'large' => [
        'title' => 'Desktop (large)',
        'selectedRatio' => 'NaN',
        'allowedAspectRatios' => [
            '16:9' => [
                'title' => '16/9',
                'value' => 16 / 9,
            ],
            '21:9' => [
                'title' => '21/9',
                'value' => 21 / 9,
            ],
            '3:2' => [
                'title' => '3/2',
                'value' => 3 / 2,
            ],
            '4:3' => [
                'title' => '4/3',
                'value' => 4 / 3,
            ],
            '4:5' => [
                'title' => '4/5',
                'value' => 4 / 5,
            ],
            '2:3' => [
                'title' => '2/3',
                'value' => 2 / 3,
            ],
            '3:4' => [
                'title' => '3/4',
                'value' => 3 / 4,
            ],
            '5:4' => [
                'title' => '5/4',
                'value' => 5 / 4,
            ],
            '1:1' => [
                'title' => '1/1',
                'value' => 1.0,
            ],
            'NaN' => [
                'title' => 'LLL:EXT:core/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                'value' => 0.0,
            ],
        ]
    ],
    'medium' => [
        'title' => 'Tablet (medium)',
        'selectedRatio' => 'NaN',
        'allowedAspectRatios' => [
            '16:9' => [
                'title' => '16/9',
                'value' => 16 / 9,
            ],
            '21:9' => [
                'title' => '21/9',
                'value' => 21 / 9,
            ],
            '3:2' => [
                'title' => '3/2',
                'value' => 3 / 2,
            ],
            '4:3' => [
                'title' => '4/3',
                'value' => 4 / 3,
            ],
            '4:5' => [
                'title' => '4/5',
                'value' => 4 / 5,
            ],
            '2:3' => [
                'title' => '2/3',
                'value' => 2 / 3,
            ],
            '3:4' => [
                'title' => '3/4',
                'value' => 3 / 4,
            ],
            '5:4' => [
                'title' => '5/4',
                'value' => 5 / 4,
            ],
            '1:1' => [
                'title' => '1/1',
                'value' => 1.0,
            ],
            'NaN' => [
                'title' => 'LLL:EXT:core/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                'value' => 0.0,
            ],
        ]
    ],
    'small' => [
        'title' => 'Mobile (small)',
        'selectedRatio' => 'NaN',
        'allowedAspectRatios' => [
            '16:9' => [
                'title' => '16/9',
                'value' => 16 / 9,
            ],
            '21:9' => [
                'title' => '21/9',
                'value' => 21 / 9,
            ],
            '3:2' => [
                'title' => '3/2',
                'value' => 3 / 2,
            ],
            '4:3' => [
                'title' => '4/3',
                'value' => 4 / 3,
            ],
            '4:5' => [
                'title' => '4/5',
                'value' => 4 / 5,
            ],
            '2:3' => [
                'title' => '2/3',
                'value' => 2 / 3,
            ],
            '3:4' => [
                'title' => '3/4',
                'value' => 3 / 4,
            ],
            '5:4' => [
                'title' => '5/4',
                'value' => 5 / 4,
            ],
            '1:1' => [
                'title' => '1/1',
                'value' => 1.0,
            ],
            'NaN' => [
                'title' => 'LLL:EXT:core/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                'value' => 0.0,
            ],
        ]
    ]
];
