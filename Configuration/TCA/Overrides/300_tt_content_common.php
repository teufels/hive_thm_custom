<?php

/*
 * This file is part of the package buepro/container_elements.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') || die('Access denied.');

(function () {

    $typeList = 'ce_container,ce_columns2,ce_columns3,ce_columns4,ce_tabs,ce_accordion,ce_tile_unit,ce_card';

    // Remove header field
    foreach (\TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $typeList, true) as $type) {
        $showitem = $GLOBALS['TCA']['tt_content']['types'][$type]['showitem'];
        $showitem = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $showitem, true);
        $showitem = array_filter($showitem, function ($item) {
            return strpos($item, 'header') !== 0;
        });
        $GLOBALS['TCA']['tt_content']['types'][$type]['showitem'] = implode(', ', $showitem);
    }

    // Add headers palette and container options (pi_flexform)
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tt_content',
        '--palette--;;headers, --div--;Container,pi_flexform;LLL:EXT:container_elements/Resources/Private/Language/locallang.xlf:options',
        $typeList,
        'after:tx_container_parent'
    );

    /**
     * BackgroundImage4CE
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tt_content',
        '--palette--;LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.palette.backgroundimage4ce;backgroundimage4ce',
        $typeList,
        'after:layout'
    );

    /**
     * Additional Fields
     */
    $additionalColumns = [
        'tx_hiveovrttcontent_backendtitle' => [
            'label' => 'Backend Title',
            'description' => 'Backend Title (not visible in frontend)',
            'exclude' => 1,
            'config' => [
                'type' => 'input',
                'eval' => 'trim',
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ],
            ]
        ]
    ];

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'tt_content',
        $additionalColumns
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tt_content',
        'tx_hiveovrttcontent_backendtitle',
        'ce_container,ce_columns2,ce_columns3,ce_columns4,ce_tabs,ce_accordion,ce_tile_unit,ce_card',
        'before:header'
    );

})();

