<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_thm_custom', 'Configuration/TypoScript', 'HIVE>Theme');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

/***************
 * Setup EXTKEY
 * was null on Typo3 10
 ****************/
$_EXTKEY ='hive_thm_custom';
$extKey = $_EXTKEY;

call_user_func(
    function($extkey)
    {

        /********************
         * Add user TSConfig
         ********************/
        $userTsConfig = \TYPO3\CMS\Core\Utility\GeneralUtility::getUrl(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extkey) . 'Configuration/TsConfig/User/config.txt');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig($userTsConfig);

    },
    $_EXTKEY
);

call_user_func(
    function($extkey)
    {

        /********************
         * Add page TSConfig
         ********************/
        $pageTsConfig = \TYPO3\CMS\Core\Utility\GeneralUtility::getUrl(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extkey) . 'Configuration/TsConfig/Page/config.txt');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig($pageTsConfig);

    },
    $_EXTKEY
);

/********************
 * Backend CSS Styling
 ********************/
$GLOBALS['TBE_STYLES']['skins'][$_EXTKEY]['stylesheetDirectories'][] = 'EXT:hive_thm_custom/Resources/Public/Css/Backend/';